# Основное:
**CTReport** - серверный модуль для приватного мода ClientTweaker (WindCheck), позволяющий
получить скриншот-жалобу от игрока на игрока.
# Зависимости:
* Events https://gitlab.com/ctmodules/events
* ClientTweaker[2.0.0,)
* CTDiscord[1.0,) https://gitlab.com/ctmodules/ctdiscord

# Установка:
* Скачать мод с репозитория
* Закинуть мод в папку *mods*

# Настройка:
Конфиг мода будет находиться по пути config/ctdatabase.cfg \
Параметры:
 * enabled - Включён ли мод в данный момент.
 
 * mainCommand - Название главной команды, которая будет использоваться для игроков.
 
 * aliases - Альтернативные команды.
 
 * cooldown - Кулдаун для игроков.
 
 * whitelistPC - Белый лист игроков, на которых нельзя пожаловаться

# Клонирование репозитория:
 Выполнить сразу после клонирования проекта: git submodule update --init \
 
 Для обновления модуля Event: git submodule update --remote