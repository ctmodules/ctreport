package ru.will0376.ctmodules.ctreport.utils;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.will0376.ctmodules.ctreport.Ctreport;
import ru.will0376.ctmodules.events.EventRequestScreen;

import java.util.ArrayList;

public class CTRStructure {
	//public EntityPlayerMP player;
	//public EntityPlayerMP customer;
	public String playerNick;
	public String customerNick;
	public long time;

	public CTRStructure(String player, String customer) throws Exception {
		if (player == null) throw new NullPointerException("Player not found");
		if (checkWhiteList(customer, playerNick)) throw new Exception("You have no rights to this action!");
		this.time = System.currentTimeMillis();
		this.playerNick = player;
		this.customerNick = customer;
	}

	private static EntityPlayerMP getEPMP(String nick) {
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers())
			if (player.getName().equals(nick)) return player;
		return null;
	}

	/**
	 * return true if cooldown enable.
	 */
	public boolean cooldown() {
		if (customerNick.equals("Server")) return false;
		if (getEPMP(customerNick).canUseCommand(4, "ctmodules.report.admin.cooldownbypass")) return false;
		return time + (Ctreport.config.getTimeout() * 60000) > System.currentTimeMillis();
	}

	public void makeScreen() {
		MinecraftForge.EVENT_BUS.post(new EventRequestScreen(customerNick, playerNick, false, true));
	}

	/**
	 * @return true, if player in WL
	 */
	private boolean checkWhiteList(String customer, String player) {
		if (customer.equals("Server")) return false;
		if (getEPMP(customer).canUseCommand(4, "ctmodules.report.admin.reportwlbypass")) return false;
		ArrayList<String> whitelist = (ArrayList<String>) Ctreport.config.getWhitelistPC();
		return whitelist.contains(player);
	}

}
