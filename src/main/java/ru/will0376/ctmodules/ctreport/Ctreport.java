package ru.will0376.ctmodules.ctreport;

import net.minecraft.launchwrapper.Launch;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import ru.will0376.ctmodules.ctreport.utils.CTRStructure;
import ru.will0376.ctmodules.events.EventLogPrint;
import ru.will0376.ctmodules.events.EventModStatusResponse;

import java.util.HashMap;

@Mod(
		modid = Ctreport.MOD_ID,
		name = Ctreport.MOD_NAME,
		version = Ctreport.VERSION,
		dependencies = "required-after:events@[1.0,)",
		acceptableRemoteVersions = "*",
		serverSideOnly = true
)
public class Ctreport {

	public static final String MOD_ID = "ctreport";
	public static final String MOD_NAME = "Ctreport";
	public static final String VERSION = "1.0";
	public static boolean DEBUG = true;
	public static Config config;
	public static HashMap<String, CTRStructure> PCCooldown = new HashMap<>(); //user,pojo.


	@Mod.Instance(MOD_ID)
	public static Ctreport INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (DEBUG) DEBUG = (boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, TextFormatting.GOLD + "[Ctreport]" + TextFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}

	@Mod.EventHandler
	public void serverStarting(FMLServerStartingEvent e) {
		e.registerServerCommand(new Commands());
	}
}
