package ru.will0376.ctmodules.ctreport;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.will0376.ctmodules.ctreport.utils.CTRStructure;
import ru.will0376.ctmodules.events.utils.ChatForm;
import ru.will0376.ctmodules.events.utils.Utils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Commands extends CommandBase {
	public static String usage = " /" + Ctreport.config.getMainCommand() + " <player>";
	public static String aliases = " Aliases: [" + String.join(", ", Ctreport.config.getAliases()) + "]";
	public static String permissions = " Permissions: [\n" +
			"   ctmodules.report.admin.cooldownbypass\n" +
			"   ctmodules.report.admin.reportwlbypass\n" +
			"   ctmodules.report.admin.blacklist\n" +
			" ]";

	@Override
	public String getName() {
		return Ctreport.config.getMainCommand();
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return usage;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (Ctreport.config.isEnabled())
			try {
				if (args.length == 0) {
					sender.sendMessage(new TextComponentString(ChatForm.prefix + Utils.makeUsage(Ctreport.MOD_ID, Commands.usage, Commands.aliases, "")));
					return;
				}
				if (sender.canUseCommand(4, "ctmodules.report.admin.blacklist")
						&& !sender.canUseCommand(4, "ctmodules.report.admin.reportwlbypass")) {
					sender.sendMessage(new TextComponentString(ChatForm.report_prefix_error + TextFormatting.RED + "You're on the blacklist!"));
					return;
				}
				if (Ctreport.PCCooldown.containsKey(sender.getName())
						&& Ctreport.PCCooldown.get(sender.getName()).cooldown()) {
					sender.sendMessage(new TextComponentString(ChatForm.report_prefix_error + String.format("Cooldown: %s sec",
							(Ctreport.PCCooldown.get(sender.getName()).time + (Ctreport.config.getTimeout() * 60000) - System.currentTimeMillis()) / 1000)));
					return;
				}

				CTRStructure pc = new CTRStructure(args[0], sender.getName());
				Ctreport.PCCooldown.put(sender.getName(), pc);
				pc.makeScreen();
				sender.sendMessage(new TextComponentString(ChatForm.report_prefix + "Thanks!"));
			} catch (Exception e) {
				sender.sendMessage(new TextComponentString(ChatForm.report_prefix_error + TextFormatting.RED + e.getMessage()));
			}
	}

	@Override
	public List<String> getAliases() {
		ArrayList<String> list = new ArrayList<>(Ctreport.config.getAliases());
		if (list.isEmpty()) return new ArrayList<>();
		return list;
	}


	@Override
	public List getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		return args.length == 1 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
	}
}
