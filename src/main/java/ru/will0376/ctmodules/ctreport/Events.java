package ru.will0376.ctmodules.ctreport;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.ctmodules.events.EventPrintHelp;
import ru.will0376.ctmodules.events.EventReloadConfig;
import ru.will0376.ctmodules.events.utils.Logger;
import ru.will0376.ctmodules.events.utils.Utils;

@Mod.EventBusSubscriber
public class Events {
	@SubscribeEvent
	public static void printHelp(EventPrintHelp e) {
		e.printToPlayer(Utils.makeUsage(Ctreport.MOD_ID, Commands.usage, Commands.aliases, Commands.permissions));
	}

	@SubscribeEvent
	public static void reload(EventReloadConfig e) {
		Ctreport.config.launch();
		if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[Ctreport]", "Reloaded");
		else
			Utils.getEPMP(e.getSender()).sendMessage(new TextComponentString(TextFormatting.GOLD + "[Ctreport] Reloaded"));
	}
}