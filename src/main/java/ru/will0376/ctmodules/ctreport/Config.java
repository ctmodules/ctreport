package ru.will0376.ctmodules.ctreport;

import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Config {
	private Configuration configuration;
	private String REPORT = "Report";
	private boolean enabled;
	private String mainCommand;
	private List<String> whitelistPC;
	private List<String> aliases;
	private int timeout;

	public Config(File file) {
		this.configuration = new Configuration(file);
	}

	public void launch() {
		load();
		setConfigs();
		save();
	}

	private void load() {
		this.configuration.load();
	}

	private void save() {
		this.configuration.save();
	}

	private void setConfigs() {
		mainCommand = configuration.getString("MainCommand", REPORT, "report", "Main command");
		aliases = Arrays.asList(configuration.getStringList("aliases", REPORT, new String[]{"rp", "repo",}, ""));
		timeout = configuration.getInt("cooldown", REPORT, 10, 0, Integer.MAX_VALUE, "WBSTimeout(in min)");
		whitelistPC = Arrays.asList(configuration.getStringList("whitelistPC", REPORT, new String[]{"will0376", "Player2"}, "Syntax: \"Nick\"."));
		enabled = configuration.getBoolean("usePC", REPORT, false, "Use /report module?");
	}

	public boolean isEnabled() {
		return enabled;
	}

	public String getMainCommand() {
		return mainCommand;
	}

	public List<String> getWhitelistPC() {
		return whitelistPC;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public int getTimeout() {
		return timeout;
	}
}
